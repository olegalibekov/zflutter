import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:z_flutter_example/examples/tap_test.dart';
import 'package:zflutter/zflutter.dart';

import 'examples/examples.dart';
import 'examples/getting_started.dart';
import 'flutter_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Colors.grey[200],
          appBarTheme: AppBarTheme(elevation: 0),
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        routes: {
          ...[
            ...Examples.list,
            ...BasicSamples.list,
            ...GettingStartedSamples.list
          ].asMap().map(
                (key, example) => MapEntry(
                  example.route,
                  (context) => Scaffold(
                    backgroundColor: example.backgroundColor,
                    appBar: example.route == '/logo'
                        ? null
                        : AppBar(
                            centerTitle: true,
                            title: Text(example.title),
                          ),
                    body: MadeWithFlutterContainer(
                      child: example.builder(context),
                      logoStyle: example.logoStyle ?? FlutterLogoColor.original,
                    ),
                  ),
                ),
              ),
          // Demos for the website
          ...[
            ...Examples.list,
            ...BasicSamples.list,
            ...GettingStartedSamples.list
          ].asMap().map((key, example) => MapEntry(
                '/demo${example.route}',
                (context) => Container(
                    color: example.backgroundColor ?? Color(0xffF5F6FA),
                    child: example.builder(context)),
              )),
        },
//        home: TapTest());
        home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    box() {
      double myWidth = 200;
      double myHeight = 500;
      return ZDragDetector(builder: (context, contoller) {
        return ZIllustration(
          children: [
            ZPositioned(
              rotate: contoller.rotate,
//                entry: [3,2,.003],
                child: ZGroup(sortMode: SortMode.inherit, children: [
              ZPositioned(
                  rotate: ZVector(0, 0, 0),
                  translate: ZVector(0, 0, myHeight * 3 / 2),
                  child: ZToBoxAdapter(
                      width: myWidth * 2 / 3,
                      height: myWidth * 2 / 3,
//                child: Image.asset(wall),
                      child: Container(
                        color: Colors.purple,
                      ))),
              ZPositioned(
                  rotate: ZVector(0, 0, 0),
                  translate: ZVector(0, 0, -myHeight * 3 / 2),
                  child: ZToBoxAdapter(
                      width: myWidth * 2 / 3,
                      height: myWidth * 2 / 3,
//                child: Image.asset(wall),
                      child: Container(
                        color: Colors.amber,
                      ))),
              ZPositioned(
                  rotate: ZVector(0, pi / 2, 0),
                  translate: ZVector(myWidth / 2, 0, 0),
                  child: ZToBoxAdapter(
                      width: myHeight,
                      height: myWidth,
//                child: Image.asset(wall),
                      child: Container(
                        color: Colors.red,
                      ))),
              ZPositioned(
                  rotate: ZVector(0, pi / 2, 0),
                  translate: ZVector(-myWidth / 2, 0, 0),
                  child: ZToBoxAdapter(
                      width: myHeight,
                      height: myWidth,
//                child: Image.asset(wall),
                      child: Container(
                        color: Colors.blue,
                      ))),
              ZPositioned(
                  rotate: ZVector(pi / 2, 0, pi / 2),
                  translate: ZVector(0, myWidth / 2, 0),
                  child: ZToBoxAdapter(
                      width: myHeight,
                      height: myWidth,
//                child: Image.asset(wall),
                      child: Container(
                        color: Colors.green,
                      ))),
              ZPositioned(
                  rotate: ZVector(pi / 2, 0, pi / 2),
                  translate: ZVector(0, -myWidth / 2, 0),
                  child: ZToBoxAdapter(
                      width: myHeight,
                      height: myWidth,
//                child: Image.asset(wall),
                      child: Container(
                        color: Colors.limeAccent,
                      ))),
            ])),
          ],
        );
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Zflutter'),
      ),
      body: box(),
    );
  }

  List<Example> get examples =>
      [...Examples.list, ...BasicSamples.list, ...GettingStartedSamples.list];
}
