import 'dart:math';

import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

import 'point_3d.dart';

class Shapes {
  final Point3D Q = Point3D(0, 0, 0);
  final Point3D O = Point3D(0, 0, 0);

  List<ZShape> quadrilateralSides(double a, double b) {
    List<ZShape> planes = [];
    ZShape plane = ZShape(path: [
      ZMove.only(x: Q.x, y: Q.y, z: Q.z),
      ZLine.only(x: Q.x, y: Q.y + a, z: Q.z),
      ZLine.only(x: Q.x + b, y: Q.y + a, z: Q.z),
      ZLine.only(x: Q.x + b, y: Q.y, z: Q.z),
    ], closed: true, stroke: 1, fill: true, color: Colors.blue);
    planes.add(plane);
    return planes;
  }

  List<ZShape> quadrilateralPoints(Point3D A, Point3D B, Point3D C, Point3D D,
      {bool filling = false, double scenario = 0}) {
    List<ZShape> planes = [];

    final Gradient gradient = RadialGradient(colors: <Color>[
      Colors.yellow.withOpacity(1.0),
      Colors.yellow.withOpacity(.0)
    ], stops: [
      0.0,
      1
    ]);
    Rect rect = Rect.fromCircle(center: Offset(-30, 0), radius: 60);
    Shader shader = gradient.createShader(rect);

    ZShape plane = ZShape(
        path: [
          ZMove.only(
              x: A.x + (scenario * scenario * 80),
              y: A.y - (scenario * scenario * 100 + scenario * 50),
              z: A.z),
          ZLine.only(
              x: B.x + (scenario * scenario * 80),
              y: B.y - (scenario * scenario * 100 + scenario * 50),
              z: B.z),
          ZLine.only(
              x: C.x + (scenario * scenario * 80),
              y: C.y - (scenario * scenario * 100 + scenario * 50),
              z: C.z + scenario * 30),
          ZLine.only(
              x: D.x + (scenario * scenario * 80),
              y: D.y - (scenario * scenario * 100 + scenario * 50),
              z: D.z + scenario * 30),
        ],
        closed: true,
        stroke: 0,
        fill: true,
        color: Colors.deepOrange,
        shader: shader
        );
    planes.add(plane);
    return planes;
  }

  List<ZShape> parallelepipedSides(Point3D O,
      {double a = 10,
      double b = 10,
      double c = 10,
      double scenario = 0,
      double up = 0,
      double right = 0,
      double forward = 0,
      double radius = 0,
      double alpha = 0,
      double beta = 0,
      double gama = 0}) {
    List<ZShape> carcasses = [];

    double X =
        O.x + right * scenario - (radius - radius * cos(2 * pi * scenario));
    double Y = O.y - up * scenario - radius * sin(2 * pi * scenario);
    double Z = O.z + forward * scenario;

    ZShape carcass = ZShape(path: [
      ZMove.only(x: (X), y: Y, z: Z),
      ZLine.only(x: (X), y: Y + b, z: Z),
      ZLine.only(x: (X + a), y: Y + b, z: Z),
      ZLine.only(x: (X + a), y: Y, z: Z),
      ZLine.only(x: (X), y: Y, z: Z),
      ZMove.only(x: (X), y: Y, z: Z + c),
      ZLine.only(x: (X), y: Y + b, z: Z + c),
      ZLine.only(x: (X + a), y: Y + b, z: Z + c),
      ZLine.only(x: (X + a), y: Y, z: Z + c),
      ZLine.only(x: (X), y: Y, z: Z + c),
      ZMove.only(x: (X), y: Y, z: Z + c),
      ZLine.only(x: (X), y: Y, z: Z),
      ZMove.only(x: (X), y: Y + b, z: Z + c),
      ZLine.only(x: (X), y: Y + b, z: Z),
      ZMove.only(x: (X + a), y: Y + b, z: Z + c),
      ZLine.only(x: (X + a), y: Y + b, z: Z),
      ZMove.only(x: (X + a), y: Y, z: Z + c),
      ZLine.only(x: (X + a), y: Y, z: Z),
    ], closed: false, stroke: 0, fill: false, color: Colors.blue);
    carcasses.add(carcass);
    return carcasses;
  }

  List<ZShape> parallelepipedFull(Point3D O,
      {double a = 10,
      double b = 10,
      double c = 10,
      double scenario = 0,
      double up = 0,
      double right = 0,
      double forward = 0,
      double radius = 0}) {
    double X =
        O.x + right * scenario - (radius - radius * cos(2 * pi * scenario));
    double Y = O.y - up * scenario - radius * sin(2 * pi * scenario);
    double Z = O.z + forward * scenario;

    List<ZShape> parallelepiped = [];
    parallelepiped.addAll(parallelepipedSides(O,
        a: a,
        b: b,
        c: c,
        scenario: scenario,
        up: up,
        right: right,
        forward: forward,
        radius: radius));
    Point3D A1 = Point3D(X, Y, Z);
    Point3D B1 = Point3D(X, Y + b, Z);
    Point3D C1 = Point3D(X + a, Y + b, Z);
    Point3D D1 = Point3D(X + a, Y, Z);

    Point3D A2 = Point3D(X, Y, Z + c);
    Point3D B2 = Point3D(X, Y + b, Z + c);
    Point3D C2 = Point3D(X + a, Y + b, Z + c);
    Point3D D2 = Point3D(X + a, Y, Z + c);

    parallelepiped.addAll(quadrilateralPoints(A1, B1, C1, D1, filling: true));
    parallelepiped.addAll(quadrilateralPoints(A1, B1, B2, A2, filling: true));
    parallelepiped.addAll(quadrilateralPoints(A2, B2, C2, D2, filling: true));
    parallelepiped.addAll(quadrilateralPoints(C2, D2, D1, C1, filling: true));
    parallelepiped.addAll(quadrilateralPoints(C2, B2, B1, C1, filling: true));
    parallelepiped.addAll(quadrilateralPoints(A2, B2, C2, D2, filling: true));
    parallelepiped.addAll(quadrilateralPoints(A2, D2, D1, A1, filling: true));

    return (parallelepiped);
  }
}
