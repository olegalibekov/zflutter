import 'dart:math';
import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

import 'point_3d.dart';

class ShineSource {
  //Static shining point
  final Point3D I;

  ShineSource([this.I]);

  ZShape pathShape(List<Point3D> path) {
    ZShape pathShape = ZShape(path: [
      for (Point3D point in path) ZLine.only(x: point.x, y: point.y, z: point.z)
    ], closed: true, stroke: 0, fill: true, color: Colors.yellow);
    return pathShape;
  }

  // Creating plane object, returns ready Zshape
  ZShape plane(Point3D A, Point3D B) {
    Point3D strokeA = Point3D().strokePoint(A, I);
    Point3D strokeB = Point3D().strokePoint(B, I);
    ZShape plane = ZShape(path: [
      ZMove.only(x: A.x, y: A.y, z: A.z),
      ZLine.only(x: B.x, y: B.y, z: B.z),
      ZLine.only(x: strokeB.x, y: strokeB.y, z: strokeB.z),
      ZLine.only(x: strokeA.x, y: strokeA.y, z: strokeA.z),
    ], closed: true, stroke: 0, fill: true, color: Colors.yellow);

    return plane;
  }

  // Creating a list of planes
  ConeValues arcCone( double radius, Point3D center, [double alphaStart = 0, double alpha = 360, double phi = 0, double theta = 0] ) {
    List<ZShape> cone = [];
    List<Point3D> arcFirstBasis = [];
    List<Point3D> arcSecondBasis = [];
    double miniAlpha;

    int countOfPlanes = 0;

    int way = 1;
    if (alpha < alphaStart) {
      way = -1;
    }

    miniAlpha = alphaStart;
    for (double i = 0; i <= (alpha - alphaStart).abs(); i++) {
      arcFirstBasis.add(Point3D(
          (radius * sin(miniAlpha * 2 * pi / 360) * cos(phi * 2 * pi / 360)) +
              center.x,

          (radius * cos(miniAlpha * 2 * pi / 360) * cos(theta * 2 * pi / 360) ) +
              center.y,

          (radius * sin(miniAlpha * 2 * pi / 360) * sin(phi * 2 * pi / 360) * sin(theta * 2 * pi / 360) ) +
              center.z));

      countOfPlanes++;
      miniAlpha += way;
    }

    arcFirstBasis.add(Point3D(
        (radius * sin(alpha * 2 * pi / 360) * cos(phi * 2 * pi / 360)) +
            center.x,
        (radius * cos(alpha * 2 * pi / 360) * cos(theta * 2 * pi / 360) ) + center.y,
        (radius * sin(alpha * 2 * pi / 360) * sin(phi * 2 * pi / 360) * sin(theta * 2 * pi / 360) ) +
            center.z));

    int last = 1;

    for (int i = 0; i < arcFirstBasis.length - 8; i += 8) {
      //print ('Base1: ${arcBasis[i].x} , ${arcBasis[i].y} ; Base2: ${arcBasis[i+8].x} , ${arcBasis[i+8].y} \n');
      cone.add(plane(arcFirstBasis[i], arcFirstBasis[i + 8]));
      last = i + 8;
    }
    //print ('Base1: ${arcBasis[last].x} , ${arcBasis[last].y} ; Base2: ${arcBasis[count].x} , ${arcBasis[count].y} \n');
    cone.add(plane(arcFirstBasis[last], arcFirstBasis[countOfPlanes]));

    arcFirstBasis.forEach((element) {
      arcSecondBasis.add(Point3D().strokePoint(element, I));
    });

    return ConeValues(
        coneShape: cone,
        firstBasisPoints: arcFirstBasis,
        secondBasisPoints: arcSecondBasis);
  }
}

class ConeValues {
  final coneShape;
  final firstBasisPoints;
  final secondBasisPoints;

  ConeValues({this.coneShape, this.firstBasisPoints, this.secondBasisPoints});
}
