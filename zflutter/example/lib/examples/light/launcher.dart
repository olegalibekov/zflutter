import 'package:flutter/material.dart';
import 'package:z_flutter_example/examples/light/point_3d.dart';
import 'package:zflutter/zflutter.dart';

import 'shapes.dart';
import 'way.dart';

class PageRenderer extends StatefulWidget {
  @override
  _PageRendererState createState() => _PageRendererState();
}

double lightRadiusValue = 300.0;
//List<double> translateValues = [0.0, 0.0];

class _PageRendererState extends State<PageRenderer>
    with SingleTickerProviderStateMixin {
  List<List<Point3D>> circle = [];
  List<List<Point3D>> circleStroke = [];
  List<bool> toggleOptions = [false, false];
  List shapeList = [];

  Point3D A;
  Point3D B;
  Point3D C;
  Point3D D;
  Point3D strokeA;
  Point3D strokeB;
  Point3D strokeC;
  Point3D strokeD;
  Point3D I;
  Point3D I22;
  Point3D ps;
  Point3D ps1;
  Point3D ps2;
  Point3D center;
  Point3D center1;
  Point3D center2;
  Point3D A2;
  double radius;

  List<Way> choreographer = [];

  List<List<ZShape>> objects = [];

  double zoomValue = 0.5;
  double zoomSliderValue = 0.0;
  double lightRadiusSliderValue = 0.0;

  List<ZShape> temporaryList = [];

  @override
  void initState() {
    // Creating base Points
    plane();

    super.initState();
  }

//    temporaryList.add(Shapes().quadrilateralPoints(A, B, C, D, scenario: 0.3));
  //shapeList.add( Shapes().quadrilateralSides(10, 20) );
  plane() {
    A = Point3D(0, 0, 0);
    B = Point3D(-50, 0, 0);
    C = Point3D(-50, 40, -10);
    D = Point3D(0, 40, -10);
    I = Point3D(0, 0, -100);
    I22 = Point3D(0, 0, -200);
    A2 = Point3D(50, 50, 50);

    strokeA = Point3D().strokePoint(A, I22);
    strokeB = Point3D().strokePoint(B, I22);
    strokeC = Point3D().strokePoint(C, I22);
    strokeD = Point3D().strokePoint(D, I22);

    radius = 40;
    ps = Point3D(40, 0, 0);
    center = Point3D(0, 0, 0);
    ps1 = Point3D(-50, -40, 0);
    ps2 = Point3D(50, 40, 0);
    center1 = Point3D(-50, 0, 0);
    center2 = Point3D(130, 0, 0);

    shapeList.addAll(Shapes().quadrilateralPoints(A, B, C, D, scenario: 0));
    shapeList.addAll(Shapes().parallelepipedFull(A,
        a: 40,
        b: 40,
        c: 10,
        scenario: 0,
        up: 90,
        right: 0,
        forward: 80,
        radius: 60));

    temporaryList.addAll(Shapes().parallelepipedFull(A,
        a: 90,
        b: 30,
        c: 60,
        scenario: 0.3,
        up: 90,
        right: 0,
        forward: 80,
        radius: 60));
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Container(
          color: Colors.cyan,
          child: Stack(children: [
            ZDragDetector(builder: (context, controller) {
              return ZIllustration(zoom: 1, children: [
                ZPositioned(
                    rotate: ZVector.only(
                        x: controller.rotate.x, y: controller.rotate.y),
                    child: ZGroup(
                        children: [for (dynamic shape in shapeList) shape]))
              ]);
            }),
            SizedBox(
                width: double.infinity,
                height: MediaQuery.of(context).size.height / 2,
                child: ZDragDetector(builder: (context, controller) {
                  return ZIllustration(zoom: 1, children: [
                    ZPositioned(
                        rotate: ZVector.only(
                            x: controller.rotate.x, y: controller.rotate.y),
                        child: ZGroup(children: [
                          for (dynamic shape in temporaryList) shape
                        ]))
                  ]);
                })),
          ]),
        ));
  }
}
