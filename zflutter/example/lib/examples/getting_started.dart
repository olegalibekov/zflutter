import 'dart:math';

import 'package:flutter/material.dart';
import 'package:z_flutter_example/spin.dart';
import 'package:zflutter/zflutter.dart';

import 'examples.dart';

class GettingStartedSamples {
  static final Example zoom = Example(
    title: 'Zoom',
    route: '/zoom',
    builder: (_) => ZIllustration(
      zoom: 2,
      children: [
        ZCircle(
          diameter: 80,
          stroke: 20,
          color: Color(0xFFCC2255),
        ),
      ],
    ),
  );

  static final Example animated = Example(
    title: 'Animated',
    route: '/animated',
    builder: (_) => Spin(
      builder: (context, rotate) => ZIllustration(
        zoom: 2,
        children: [
          ZPositioned(
            rotate: rotate,
            child: ZCircle(
              diameter: 80,
              stroke: 20,
              color: Color(0xFFCC2255),
            ),
          ),
        ],
      ),
    ),
  );

  static final Example drag = Example(
    title: 'Drag',
    route: '/drag',
    builder: (_) => ZDragDetector(
      builder: (context, controller) {
        return ZIllustration(
          children: [
            ZPositioned(
              rotate: controller.rotate,
              child: ZCircle(
                diameter: 80,
                stroke: 20,
                color: Color(0xFFCC2255),
              ),
            ),
          ],
        );
      },
    ),
  );

  static final Example boxAdapter = Example(
    title: 'Box Adapter',
    route: '/box_adapter',
    builder: (_) => ZDragDetector(
      builder: (context, controller) {
        return ZIllustration(
          zoom: 4,
          children: [
            ZPositioned(
              rotate: controller.rotate + ZVector(0, pi/2, 0),

              child: ZPositioned.entry(
                entry: [3,2,0.06],
                child: ZToBoxAdapter(
                  height: 80,
                  width: 80,
                  child: GestureDetector(
                      onTap: () {
                        print("object");
                      },
                      child: Container(
                        color: Color(0xFFCC2255),
                        child: Image.network(
                            "https://media.wired.com/photos/5a593a7ff11e325008172bc2/master/w_2560%2Cc_limit/pulsar-831502910.jpg"),
                      )),
                ),
              ),
            ),
          ],
        );
      },
    ),
  );

  static List<Example> get list => [zoom, animated, drag, boxAdapter];
}
