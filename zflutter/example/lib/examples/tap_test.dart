import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

class TapTest extends StatefulWidget {
  @override
  _TapTestState createState() => _TapTestState();
}

class _TapTestState extends State<TapTest> {
  double angleX = 0.0;
  double angleY = 0.0;
  double angleZ = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ZIllustration(children: [
      ZPositioned(
          rotate: ZVector(angleX, angleY, angleZ),
          child: ZToBoxAdapter(
              width: 200,
              height: 200,
              child: GestureDetector(
                  onPanUpdate: _drag, child: Container(color: Colors.blue))))
    ]));
  }

  _drag(DragUpdateDetails update) {
    setState(() {
      angleX += update.delta.dy;
      if (angleX > 360)
        angleX = angleX - 360;
      else if (angleX < 0) angleX = 360 - angleX;

      angleY += update.delta.dx;
      if (angleY > 360)
        angleY = angleY - 360;
      else if (angleY < 0) angleY = 360 - angleY;
    });
  }
}
