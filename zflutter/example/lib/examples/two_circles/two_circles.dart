import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

class TwoCircles extends StatefulWidget {
  @override
  _TwoCirclesState createState() => _TwoCirclesState();
}

class _TwoCirclesState extends State<TwoCircles> with TickerProviderStateMixin {
  AnimationController controller;
  Animation tween;

  @override
  initState() {
    // After 150 visibility = 0
    ZVector.zVisible = 150;
    ZVector.zNotVisible = -150;
    // 150 - (1 - 0.4) visibility = 1; after is decreasing
    ZVector.zVisibleX = 0.1;
    //Must be absolute
    ZVector.zStartAnimValueAbs = (-200.0).abs();

    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    tween = Tween<double>(begin: -200, end: 200).animate(controller);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Two circles')),
        body: AnimatedBuilder(
            animation: controller,
            builder: (context, _) {
//              print(tween.value);
              return ZDragDetector(
                  builder: (BuildContext context, ZDragController controller) {
                return ZIllustration(children: [
                  ZPositioned(
                      rotate: controller.rotate,
                      child: ZGroup(children: [
                        ZPositioned(
                            scale: ZVector.getScale((tween.value as double)),
                            translate: ZVector(0, 0, tween.value),
                            child: ZCircle(
                                fill: true,
                                diameter: 80,
                                stroke: 1,
                                color: Color(0xFFCC2255).withOpacity(ZVector.opacityValue(tween.value)))),
                        ZPositioned(
                            scale: ZVector.getScale(0),
                            translate: ZVector(100, 0, 0),
                            child: ZCircle(
                                diameter: 80, stroke: 1, color: Colors.grey))
                      ]))
                ]);
              });
            }),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              if (controller.status == AnimationStatus.completed)
                controller.reverse();
              else
                controller.forward();
            },
            tooltip: 'Increment',
            child: Icon(Icons.add)));
  }
}
